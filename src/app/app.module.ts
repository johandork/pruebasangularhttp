import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ObjetivosComponent } from './objetivos/objetivos.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { ServicioObjetivoService } from './Servicios/servicio-objetivo.service';
@NgModule({
  declarations: [
    AppComponent,
    ObjetivosComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  
  providers: [ServicioObjetivoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
