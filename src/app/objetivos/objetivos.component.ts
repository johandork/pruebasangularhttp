import { Component, OnInit } from '@angular/core';
import { Objetivo } from '../Clases/objetivo';
import { ServicioObjetivoService } from '../Servicios/servicio-objetivo.service';
import { Posts } from '../Clases/posts';

@Component({
  selector: 'app-objetivos',
  templateUrl: './objetivos.component.html',
  styleUrls: ['./objetivos.component.css']
})
export class ObjetivosComponent implements OnInit {
  listaO:Objetivo[]
  listaPosts:Posts[]
  obj:Objetivo
  id:number
  constructor(private servO:ServicioObjetivoService) {
    //this.listar()
    this.listaO=[{des_objetivo:'hol',fecha_finalizacion:null,id_estudiante:1,id_objetivo:2,nom_objetivo:'mi obj'}]
    this.obtenerPosts()
   }

  ngOnInit(): void {
  }

  listar(){
    /*this.servO.getlistobjet().subscribe(
      data => {this.listaO=data
      console.log(data)
      }
      ,
      err=>console.log(err)
    )*/
    this.servO.getlistobjet().then(objetivos => this.listaO=objetivos)
    console.log('lista ',this.listaO)
    //console.log('posicion 0 ',this.listaO.length)
  }
  obtenerPosts(){
    this.servO.getPosts().subscribe(
      todos => {
        this.listaPosts=todos;
        console.log('datos Posts: ',todos)
      },
      err => console.error(err)
    )
  }
  Buscar(){
    if(this.id!==null)this.servO.getidobj(this.id)
  }
}
