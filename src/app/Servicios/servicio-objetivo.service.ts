import { Injectable } from '@angular/core';
import { Objetivo } from '../Clases/objetivo';
import { HttpClient} from '@angular/common/http'
import { Observable } from 'rxjs';
import { Posts } from '../Clases/posts'
@Injectable({
  providedIn: 'root'
})
export class ServicioObjetivoService {
  miO:Objetivo
  constructor(private http:HttpClient) { }
  getlistobjet():Promise<Objetivo[]>{
    return this.http.get<Objetivo[]>('http://127.0.0.1:8000/aplicacion/objetivo/')
    .toPromise().then(response => response as Objetivo[])
  }
  getidobj(id:number):Observable< Objetivo>{
    return this.http.get<Objetivo>('http://127.0.0.1:8000/aplicacion/objetivo/'+id+'/')
  }

  getPosts():Observable<Posts[]>{
    return this.http.get<Posts[]>('https://jsonplaceholder.typicode.com/posts')

  }
}
