import { TestBed } from '@angular/core/testing';

import { ServicioObjetivoService } from './servicio-objetivo.service';

describe('ServicioObjetivoService', () => {
  let service: ServicioObjetivoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicioObjetivoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
